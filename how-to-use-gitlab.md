##1.注册gitlab账户

![](./1.png "注册界面")

- 挂VPN（只有注册时需要挂VPN，否则无法出现验证码）
- Fullname可填写自己名字的拼音
- Username用户名Gitlab会检测，确保唯一
- Email和Email confirmation直接填你常用的一个就好了，Email confirmation应该用于注册验证
- 勾选人机身份验证进行验证（验证了3组图，我的是找出所有含交通指示牌的图片）
- Register提交后Gitlab会发送邮件给你填写的Email地址，在邮件中点开链接完成注册

##2.创建自己的第一个project

![](./2.png "创建project界面")

- 先创建一个空的project（Blank project）
- 填写自己的Project name
- Visibility Level 有3个等级，这里仅作测试我们选Public

1. Private只有明确授权的用户才有读写等权限
2. Internal是所有登录gitlab的用户能读（修改、提交等进一步的权限可自己设定）
3. Public是所有互联网用户能读（修改、提交等进一步的权限可自己设定）

##3.Gitlab与本机互操作

![](./3.png "Gitlab与本机互操作")

当创建一个Project后进入该Project界面，Gitlab会告诉你与本地文件互操作的相关命令，你可以在自己的Windows或Linux环境下使用Gitlab。

同时，你也会注意到web界面上方提示你添加ssh公钥到Gitlab，你可以点进去添加你的ssh公钥，或者从右上方你的绿色头像进入settings，找到SSH keys，粘贴你的id_rsa.pub文件里的内容。

- Git global setup(配置你的计算机上的git与Gitlab上的信息一致)
- Create a new repository(将Gitlab上的仓库导入到本地计算机，并创建一个文件，将该文件同步到Gitlab上)
- Existing folder(将已有的本地文件同步到Gitlab)
- Existing Git repository(将已有的本地Git仓库同步到Gitlab)




